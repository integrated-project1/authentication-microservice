# openapi_client.DefaultApi

All URIs are relative to *http://localhost/1.0.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**delete_client**](DefaultApi.md#delete_client) | **DELETE** /clients/{client_id} | Delete a client
[**delete_me**](DefaultApi.md#delete_me) | **DELETE** /me | Delete me
[**get_client_info**](DefaultApi.md#get_client_info) | **GET** /clients/{client_id} | Get a client&#39;s data
[**get_clients**](DefaultApi.md#get_clients) | **GET** /clients | Get the list of the clients
[**get_key**](DefaultApi.md#get_key) | **GET** /key | Get the public key
[**get_login_form**](DefaultApi.md#get_login_form) | **GET** /login | Get form for logging in
[**get_my_info**](DefaultApi.md#get_my_info) | **GET** /me | Get my data
[**login**](DefaultApi.md#login) | **POST** /login | Authentication, ask for a JWT
[**register**](DefaultApi.md#register) | **POST** /clients | Register a new client
[**update_client_info**](DefaultApi.md#update_client_info) | **PUT** /clients/{client_id} | Update a client&#39;s data
[**update_my_info**](DefaultApi.md#update_my_info) | **PUT** /me | Update a my data


# **delete_client**
> delete_client(client_id)

Delete a client

### Example

* OAuth Authentication (Oauth2):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost/1.0.0
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost/1.0.0"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: Oauth2
configuration = openapi_client.Configuration(
    host = "http://localhost/1.0.0"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.DefaultApi(api_client)
    client_id = 'xXxDarkSasukeKingSlayerdu62xXx' # str |

    try:
        # Delete a client
        api_instance.delete_client(client_id)
    except ApiException as e:
        print("Exception when calling DefaultApi->delete_client: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **client_id** | **str**|  |

### Return type

void (empty response body)

### Authorization

[Oauth2](../README.md#Oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Client successfully deleted |  -  |
**401** | Access token is missing or invalid |  -  |
**403** | Unauthorized operation |  -  |
**204** | Client does not exist |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_me**
> delete_me()

Delete me

### Example

* OAuth Authentication (Oauth2):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost/1.0.0
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost/1.0.0"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: Oauth2
configuration = openapi_client.Configuration(
    host = "http://localhost/1.0.0"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.DefaultApi(api_client)

    try:
        # Delete me
        api_instance.delete_me()
    except ApiException as e:
        print("Exception when calling DefaultApi->delete_me: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

[Oauth2](../README.md#Oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Client successfully deleted |  -  |
**401** | Access token is missing or invalid |  -  |
**403** | Unauthorized operation |  -  |
**204** | Client does not exist |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_client_info**
> Client get_client_info(client_id)

Get a client's data

### Example

* OAuth Authentication (Oauth2):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost/1.0.0
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost/1.0.0"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: Oauth2
configuration = openapi_client.Configuration(
    host = "http://localhost/1.0.0"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.DefaultApi(api_client)
    client_id = 'xXxDarkSasukeKingSlayerdu62xXx' # str |

    try:
        # Get a client's data
        api_response = api_instance.get_client_info(client_id)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->get_client_info: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **client_id** | **str**|  |

### Return type

[**Client**](Client.md)

### Authorization

[Oauth2](../README.md#Oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful operation |  -  |
**401** | Access token is missing or invalid |  -  |
**403** | Unauthorized operation |  -  |
**204** | Client does not exist |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_clients**
> list[str] get_clients()

Get the list of the clients

### Example

* OAuth Authentication (Oauth2):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost/1.0.0
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost/1.0.0"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: Oauth2
configuration = openapi_client.Configuration(
    host = "http://localhost/1.0.0"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.DefaultApi(api_client)

    try:
        # Get the list of the clients
        api_response = api_instance.get_clients()
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->get_clients: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

**list[str]**

### Authorization

[Oauth2](../README.md#Oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Authorized operation |  -  |
**401** | Access token is missing or invalid |  -  |
**403** | Unauthorized operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_key**
> object get_key()

Get the public key

### Example

```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost/1.0.0
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost/1.0.0"
)


# Enter a context with an instance of the API client
with openapi_client.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.DefaultApi(api_client)
    
    try:
        # Get the public key
        api_response = api_instance.get_key()
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->get_key: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

**object**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_login_form**
> str get_login_form(response_type, client_id, redirect_uri=redirect_uri, scope=scope, state=state)

Get form for logging in

### Example

```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost/1.0.0
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost/1.0.0"
)


# Enter a context with an instance of the API client
with openapi_client.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.DefaultApi(api_client)
    response_type = 'token' # str | Type of the response. Must be set to \"token\" in the context of Oauth2 implicit flow
client_id = 'xXxDarkSasukeKingSlayerdu62xXx' # str | ID of the client wanting to log in
redirect_uri = 'http://example.com:123/somestuff' # str | URI to which the client will be redirected after authentication (optional)
scope = 'read:clients delete:clients register:clients read:data' # str | Space separeted list of scopes the token will have (optional)
state = 'U2F0IEp1bCAyNSAyMDIwIDEzOjMwOjIyIEdNVCswMjAwIChDZW50cmFsIEV1cm9wZWFuIFN1bW1lciBUaW1lKQ%3D%3D' # str | An opaque value used by the client to maintain state between the request and callback (optional)

    try:
        # Get form for logging in
        api_response = api_instance.get_login_form(response_type, client_id, redirect_uri=redirect_uri, scope=scope, state=state)
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->get_login_form: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **response_type** | **str**| Type of the response. Must be set to \&quot;token\&quot; in the context of Oauth2 implicit flow |
 **client_id** | **str**| ID of the client wanting to log in |
 **redirect_uri** | **str**| URI to which the client will be redirected after authentication | [optional]
 **scope** | **str**| Space separeted list of scopes the token will have | [optional]
 **state** | **str**| An opaque value used by the client to maintain state between the request and callback | [optional]

### Return type

**str**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/html

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_my_info**
> Client get_my_info()

Get my data

### Example

* OAuth Authentication (Oauth2):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost/1.0.0
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost/1.0.0"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: Oauth2
configuration = openapi_client.Configuration(
    host = "http://localhost/1.0.0"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.DefaultApi(api_client)
    
    try:
        # Get my data
        api_response = api_instance.get_my_info()
        pprint(api_response)
    except ApiException as e:
        print("Exception when calling DefaultApi->get_my_info: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**Client**](Client.md)

### Authorization

[Oauth2](../README.md#Oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful operation |  -  |
**401** | Access token is missing or invalid |  -  |
**403** | Unauthorized operation |  -  |
**204** | Client does not exist |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **login**
> login(authentication_info)

Authentication, ask for a JWT

### Example

```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost/1.0.0
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost/1.0.0"
)


# Enter a context with an instance of the API client
with openapi_client.ApiClient() as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.DefaultApi(api_client)
    authentication_info = openapi_client.AuthenticationInfo() # AuthenticationInfo | Client credentials for logging in

    try:
        # Authentication, ask for a JWT
        api_instance.login(authentication_info)
    except ApiException as e:
        print("Exception when calling DefaultApi->login: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authentication_info** | [**AuthenticationInfo**](AuthenticationInfo.md)| Client credentials for logging in |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**302** | Successfully logged in, redirecting |  * Location - Redirection uri and its corresponding fragment <br>  |
**422** | Invalid credentials |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **register**
> register(client)

Register a new client

### Example

* OAuth Authentication (Oauth2):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost/1.0.0
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost/1.0.0"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: Oauth2
configuration = openapi_client.Configuration(
    host = "http://localhost/1.0.0"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.DefaultApi(api_client)
    client = openapi_client.Client() # Client | Client credentials for registering

    try:
        # Register a new client
        api_instance.register(client)
    except ApiException as e:
        print("Exception when calling DefaultApi->register: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **client** | [**Client**](Client.md)| Client credentials for registering |

### Return type

void (empty response body)

### Authorization

[Oauth2](../README.md#Oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Client successfully registered |  -  |
**401** | Access token is missing or invalid |  -  |
**403** | Unauthorized operation |  -  |
**409** | Duplicate client |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_client_info**
> update_client_info(client_id, client)

Update a client's data

### Example

* OAuth Authentication (Oauth2):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost/1.0.0
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost/1.0.0"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: Oauth2
configuration = openapi_client.Configuration(
    host = "http://localhost/1.0.0"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.DefaultApi(api_client)
    client_id = 'xXxDarkSasukeKingSlayerdu62xXx' # str |
client = openapi_client.Client() # Client | New client data

    try:
        # Update a client's data
        api_instance.update_client_info(client_id, client)
    except ApiException as e:
        print("Exception when calling DefaultApi->update_client_info: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **client_id** | **str**|  |
 **client** | [**Client**](Client.md)| New client data |

### Return type

void (empty response body)

### Authorization

[Oauth2](../README.md#Oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful operation |  -  |
**401** | Access token is missing or invalid |  -  |
**403** | Unauthorized operation |  -  |
**204** | Client does not exist |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_my_info**
> update_my_info(client)

Update a my data

### Example

* OAuth Authentication (Oauth2):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to http://localhost/1.0.0
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost/1.0.0"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure OAuth2 access token for authorization: Oauth2
configuration = openapi_client.Configuration(
    host = "http://localhost/1.0.0"
)
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.DefaultApi(api_client)
    client = openapi_client.Client() # Client | Client credentials for registering

    try:
        # Update a my data
        api_instance.update_my_info(client)
    except ApiException as e:
        print("Exception when calling DefaultApi->update_my_info: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **client** | [**Client**](Client.md)| Client credentials for registering |

### Return type

void (empty response body)

### Authorization

[Oauth2](../README.md#Oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful operation |  -  |
**401** | Access token is missing or invalid |  -  |
**403** | Unauthorized operation |  -  |
**204** | Client does not exist |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

