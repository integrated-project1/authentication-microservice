# openapi.model.Client

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**clientId** | **String** |  | [optional] 
**clientSecret** | **String** |  | [optional] 
**scope** | **String** |  | [optional] 
**clientType** | **String** |  | [optional] 
**redirectUris** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


