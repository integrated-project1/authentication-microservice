# openapi.api.DefaultApi

## Load the API package
```dart
import 'package:openapi/api.dart';
```

All URIs are relative to *http://localhost/1.0.0*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteClient**](DefaultApi.md#deleteClient) | **DELETE** /clients/{client_id} | Delete a client
[**deleteMe**](DefaultApi.md#deleteMe) | **DELETE** /me | Delete me
[**getClientInfo**](DefaultApi.md#getClientInfo) | **GET** /clients/{client_id} | Get a client&#39;s data
[**getClients**](DefaultApi.md#getClients) | **GET** /clients | Get the list of the clients
[**getKey**](DefaultApi.md#getKey) | **GET** /key | Get the public key
[**getLoginForm**](DefaultApi.md#getLoginForm) | **GET** /login | Get form for logging in
[**getMyInfo**](DefaultApi.md#getMyInfo) | **GET** /me | Get my data
[**login**](DefaultApi.md#login) | **POST** /login | Authentication, ask for a JWT
[**register**](DefaultApi.md#register) | **POST** /clients | Register a new client
[**updateClientInfo**](DefaultApi.md#updateClientInfo) | **PUT** /clients/{client_id} | Update a client&#39;s data
[**updateMyInfo**](DefaultApi.md#updateMyInfo) | **PUT** /me | Update a my data


# **deleteClient**
> deleteClient(clientId)

Delete a client

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure OAuth2 access token for authorization: Oauth2
//defaultApiClient.getAuthentication<OAuth>('Oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

var api_instance = DefaultApi();
var clientId = xXxDarkSasukeKingSlayerdu62xXx; // String | 

try { 
    api_instance.deleteClient(clientId);
} catch (e) {
    print("Exception when calling DefaultApi->deleteClient: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **String**|  | 

### Return type

void (empty response body)

### Authorization

[Oauth2](../README.md#Oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **deleteMe**
> deleteMe()

Delete me

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure OAuth2 access token for authorization: Oauth2
//defaultApiClient.getAuthentication<OAuth>('Oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

var api_instance = DefaultApi();

try { 
    api_instance.deleteMe();
} catch (e) {
    print("Exception when calling DefaultApi->deleteMe: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

[Oauth2](../README.md#Oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getClientInfo**
> Client getClientInfo(clientId)

Get a client's data

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure OAuth2 access token for authorization: Oauth2
//defaultApiClient.getAuthentication<OAuth>('Oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

var api_instance = DefaultApi();
var clientId = xXxDarkSasukeKingSlayerdu62xXx; // String | 

try { 
    var result = api_instance.getClientInfo(clientId);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->getClientInfo: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **String**|  | 

### Return type

[**Client**](Client.md)

### Authorization

[Oauth2](../README.md#Oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getClients**
> List<String> getClients()

Get the list of the clients

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure OAuth2 access token for authorization: Oauth2
//defaultApiClient.getAuthentication<OAuth>('Oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

var api_instance = DefaultApi();

try { 
    var result = api_instance.getClients();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->getClients: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**List<String>**

### Authorization

[Oauth2](../README.md#Oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getKey**
> Object getKey()

Get the public key

### Example 
```dart
import 'package:openapi/api.dart';

var api_instance = DefaultApi();

try { 
    var result = api_instance.getKey();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->getKey: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**Object**](Object.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getLoginForm**
> String getLoginForm(responseType, clientId, redirectUri, scope, state)

Get form for logging in

### Example 
```dart
import 'package:openapi/api.dart';

var api_instance = DefaultApi();
var responseType = token; // String | Type of the response. Must be set to \"token\" in the context of Oauth2 implicit flow
var clientId = xXxDarkSasukeKingSlayerdu62xXx; // String | ID of the client wanting to log in
var redirectUri = http://example.com:123/somestuff; // String | URI to which the client will be redirected after authentication
var scope = read:clients delete:clients register:clients read:data; // String | Space separeted list of scopes the token will have
var state = U2F0IEp1bCAyNSAyMDIwIDEzOjMwOjIyIEdNVCswMjAwIChDZW50cmFsIEV1cm9wZWFuIFN1bW1lciBUaW1lKQ%3D%3D; // String | An opaque value used by the client to maintain state between the request and callback

try { 
    var result = api_instance.getLoginForm(responseType, clientId, redirectUri, scope, state);
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->getLoginForm: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **responseType** | **String**| Type of the response. Must be set to \&quot;token\&quot; in the context of Oauth2 implicit flow | 
 **clientId** | **String**| ID of the client wanting to log in | 
 **redirectUri** | **String**| URI to which the client will be redirected after authentication | [optional] 
 **scope** | **String**| Space separeted list of scopes the token will have | [optional] 
 **state** | **String**| An opaque value used by the client to maintain state between the request and callback | [optional] 

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/html

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getMyInfo**
> Client getMyInfo()

Get my data

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure OAuth2 access token for authorization: Oauth2
//defaultApiClient.getAuthentication<OAuth>('Oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

var api_instance = DefaultApi();

try { 
    var result = api_instance.getMyInfo();
    print(result);
} catch (e) {
    print("Exception when calling DefaultApi->getMyInfo: $e\n");
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**Client**](Client.md)

### Authorization

[Oauth2](../README.md#Oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **login**
> login(authenticationInfo)

Authentication, ask for a JWT

### Example 
```dart
import 'package:openapi/api.dart';

var api_instance = DefaultApi();
var authenticationInfo = AuthenticationInfo(); // AuthenticationInfo | Client credentials for logging in

try { 
    api_instance.login(authenticationInfo);
} catch (e) {
    print("Exception when calling DefaultApi->login: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authenticationInfo** | [**AuthenticationInfo**](AuthenticationInfo.md)| Client credentials for logging in | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **register**
> register(client)

Register a new client

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure OAuth2 access token for authorization: Oauth2
//defaultApiClient.getAuthentication<OAuth>('Oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

var api_instance = DefaultApi();
var client = Client(); // Client | Client credentials for registering

try { 
    api_instance.register(client);
} catch (e) {
    print("Exception when calling DefaultApi->register: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **client** | [**Client**](Client.md)| Client credentials for registering | 

### Return type

void (empty response body)

### Authorization

[Oauth2](../README.md#Oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **updateClientInfo**
> updateClientInfo(clientId, client)

Update a client's data

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure OAuth2 access token for authorization: Oauth2
//defaultApiClient.getAuthentication<OAuth>('Oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

var api_instance = DefaultApi();
var clientId = xXxDarkSasukeKingSlayerdu62xXx; // String | 
var client = Client(); // Client | New client data

try { 
    api_instance.updateClientInfo(clientId, client);
} catch (e) {
    print("Exception when calling DefaultApi->updateClientInfo: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **String**|  | 
 **client** | [**Client**](Client.md)| New client data | 

### Return type

void (empty response body)

### Authorization

[Oauth2](../README.md#Oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **updateMyInfo**
> updateMyInfo(client)

Update a my data

### Example 
```dart
import 'package:openapi/api.dart';
// TODO Configure OAuth2 access token for authorization: Oauth2
//defaultApiClient.getAuthentication<OAuth>('Oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

var api_instance = DefaultApi();
var client = Client(); // Client | Client credentials for registering

try { 
    api_instance.updateMyInfo(client);
} catch (e) {
    print("Exception when calling DefaultApi->updateMyInfo: $e\n");
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **client** | [**Client**](Client.md)| Client credentials for registering | 

### Return type

void (empty response body)

### Authorization

[Oauth2](../README.md#Oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

