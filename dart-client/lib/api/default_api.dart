part of userApiClient.api;



class DefaultApi {
  final ApiClient apiClient;

  DefaultApi([ApiClient apiClient]) : apiClient = apiClient ?? defaultApiClient;

  /// Delete a client with HTTP info returned
  ///
  /// 
  Future deleteClientWithHttpInfo(String clientId) async {
    Object postBody;

    // verify required params are set
    if(clientId == null) {
     throw ApiException(400, "Missing required param: clientId");
    }

    // create path and map variables
    String path = "/clients/{client_id}".replaceAll("{format}","json").replaceAll("{" + "client_id" + "}", clientId.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["Oauth2"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      http.MultipartRequest mp = http.MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'DELETE',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Delete a client
  ///
  ///String clientId  (required):
  ///    
  /// 
  Future deleteClient(String clientId) async {
    http.Response response = await deleteClientWithHttpInfo(clientId);
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
    } else {
      return;
    }
  }

  /// Delete me with HTTP info returned
  ///
  /// 
  Future deleteMeWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/me".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["Oauth2"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      http.MultipartRequest mp = http.MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'DELETE',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Delete me
  ///
  /// 
  Future deleteMe() async {
    http.Response response = await deleteMeWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
    } else {
      return;
    }
  }

  /// Get a client&#39;s data with HTTP info returned
  ///
  /// 
  Future<http.Response> getClientInfoWithHttpInfo(String clientId) async {
    Object postBody;

    // verify required params are set
    if(clientId == null) {
     throw ApiException(400, "Missing required param: clientId");
    }

    // create path and map variables
    String path = "/clients/{client_id}".replaceAll("{format}","json").replaceAll("{" + "client_id" + "}", clientId.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["Oauth2"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      http.MultipartRequest mp = http.MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'GET',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get a client&#39;s data
  ///
  ///String clientId  (required):
  ///    
  /// 
  Future<Client> getClientInfo(String clientId) async {
    http.Response response = await getClientInfoWithHttpInfo(clientId);
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'Client') as Client;
    } else {
      return null;
    }
  }

  /// Get the list of the clients with HTTP info returned
  ///
  /// 
  Future<http.Response> getClientsWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/clients".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["Oauth2"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      http.MultipartRequest mp = http.MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'GET',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get the list of the clients
  ///
  /// 
  Future<List<String>> getClients() async {
    http.Response response = await getClientsWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return (apiClient.deserialize(_decodeBodyBytes(response), 'List<String>') as List).map((item) => item as String).toList();
    } else {
      return null;
    }
  }

  /// Get the public key with HTTP info returned
  ///
  /// 
  Future<http.Response> getKeyWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/key".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = [];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      http.MultipartRequest mp = http.MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'GET',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get the public key
  ///
  /// 
  Future<Object> getKey() async {
    http.Response response = await getKeyWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'Object') as Object;
    } else {
      return null;
    }
  }

  /// Get form for logging in with HTTP info returned
  ///
  /// 
  Future<http.Response> getLoginFormWithHttpInfo(String responseType, String clientId, { String redirectUri, String scope, String state }) async {
    Object postBody;

    // verify required params are set
    if(responseType == null) {
     throw ApiException(400, "Missing required param: responseType");
    }
    if(clientId == null) {
     throw ApiException(400, "Missing required param: clientId");
    }

    // create path and map variables
    String path = "/login".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};
      queryParams.addAll(_convertParametersForCollectionFormat("", "response_type", responseType));
      queryParams.addAll(_convertParametersForCollectionFormat("", "client_id", clientId));
    if(redirectUri != null) {
      queryParams.addAll(_convertParametersForCollectionFormat("", "redirect_uri", redirectUri));
    }
    if(scope != null) {
      queryParams.addAll(_convertParametersForCollectionFormat("", "scope", scope));
    }
    if(state != null) {
      queryParams.addAll(_convertParametersForCollectionFormat("", "state", state));
    }

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = [];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      http.MultipartRequest mp = http.MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'GET',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get form for logging in
  ///
  ///String responseType  (required):
  ///     Type of the response. Must be set to \"token\" in the context of Oauth2 implicit flow
  ///String clientId  (required):
  ///     ID of the client wanting to log in
  ///String redirectUri :
  ///     URI to which the client will be redirected after authentication
  ///String scope :
  ///     Space separeted list of scopes the token will have
  ///String state :
  ///     An opaque value used by the client to maintain state between the request and callback
  /// 
  Future<String> getLoginForm(String responseType, String clientId, { String redirectUri, String scope, String state }) async {
    http.Response response = await getLoginFormWithHttpInfo(responseType, clientId,  redirectUri: redirectUri, scope: scope, state: state );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'String') as String;
    } else {
      return null;
    }
  }

  /// Get my data with HTTP info returned
  ///
  /// 
  Future<http.Response> getMyInfoWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/me".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["Oauth2"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      http.MultipartRequest mp = http.MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'GET',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get my data
  ///
  /// 
  Future<Client> getMyInfo() async {
    http.Response response = await getMyInfoWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'Client') as Client;
    } else {
      return null;
    }
  }

  /// Authentication, ask for a JWT with HTTP info returned
  ///
  /// 
  Future loginWithHttpInfo(AuthenticationInfo authenticationInfo) async {
    Object postBody = authenticationInfo;

    // verify required params are set
    if(authenticationInfo == null) {
     throw ApiException(400, "Missing required param: authenticationInfo");
    }

    // create path and map variables
    String path = "/login".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = [];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      http.MultipartRequest mp = http.MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Authentication, ask for a JWT
  ///
  ///AuthenticationInfo authenticationInfo  (required):
  ///     Client credentials for logging in
  /// 
  Future login(AuthenticationInfo authenticationInfo) async {
    http.Response response = await loginWithHttpInfo(authenticationInfo);
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
    } else {
      return;
    }
  }

  /// Register a new client with HTTP info returned
  ///
  /// 
  Future registerWithHttpInfo(Client client) async {
    Object postBody = client;

    // verify required params are set
    if(client == null) {
     throw ApiException(400, "Missing required param: client");
    }

    // create path and map variables
    String path = "/clients".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["Oauth2"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      http.MultipartRequest mp = http.MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Register a new client
  ///
  ///Client client  (required):
  ///     Client credentials for registering
  /// 
  Future register(Client client) async {
    http.Response response = await registerWithHttpInfo(client);
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
    } else {
      return;
    }
  }

  /// Update a client&#39;s data with HTTP info returned
  ///
  /// 
  Future updateClientInfoWithHttpInfo(String clientId, Client client) async {
    Object postBody = client;

    // verify required params are set
    if(clientId == null) {
     throw ApiException(400, "Missing required param: clientId");
    }
    if(client == null) {
     throw ApiException(400, "Missing required param: client");
    }

    // create path and map variables
    String path = "/clients/{client_id}".replaceAll("{format}","json").replaceAll("{" + "client_id" + "}", clientId.toString());

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["Oauth2"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      http.MultipartRequest mp = http.MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'PUT',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Update a client&#39;s data
  ///
  ///String clientId  (required):
  ///    
  ///Client client  (required):
  ///     New client data
  /// 
  Future updateClientInfo(String clientId, Client client) async {
    http.Response response = await updateClientInfoWithHttpInfo(clientId, client);
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
    } else {
      return;
    }
  }

  /// Update a my data with HTTP info returned
  ///
  /// 
  Future updateMyInfoWithHttpInfo(Client client) async {
    Object postBody = client;

    // verify required params are set
    if(client == null) {
     throw ApiException(400, "Missing required param: client");
    }

    // create path and map variables
    String path = "/me".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["Oauth2"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      http.MultipartRequest mp = http.MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'PUT',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Update a my data
  ///
  ///Client client  (required):
  ///     Client credentials for registering
  /// 
  Future updateMyInfo(Client client) async {
    http.Response response = await updateMyInfoWithHttpInfo(client);
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
    } else {
      return;
    }
  }

}
