part of userApiClient.api;

class Client {
  
  String clientId;
  
  String clientSecret;
  
  String scope;
  
  String clientType;
  
  String redirectUris;

  Client({
    this.clientId,
    this.clientSecret,
    this.scope,
    this.clientType,
    this.redirectUris,
  });

  @override
  String toString() {
    return 'Client[clientId=$clientId, clientSecret=$clientSecret, scope=$scope, clientType=$clientType, redirectUris=$redirectUris, ]';
  }

  Client.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    clientId = json['client_id'];
    clientSecret = json['client_secret'];
    scope = json['scope'];
    clientType = json['client_type'];
    redirectUris = json['redirect_uris'];
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = {};
    if (clientId != null)
      json['client_id'] = clientId;
    if (clientSecret != null)
      json['client_secret'] = clientSecret;
    if (scope != null)
      json['scope'] = scope;
    if (clientType != null)
      json['client_type'] = clientType;
    if (redirectUris != null)
      json['redirect_uris'] = redirectUris;
    return json;
  }

  static List<Client> listFromJson(List<dynamic> json) {
    return json == null ? List<Client>() : json.map((value) => Client.fromJson(value)).toList();
  }

  static Map<String, Client> mapFromJson(Map<String, dynamic> json) {
    final map = Map<String, Client>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = Client.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of Client-objects as value to a dart map
  static Map<String, List<Client>> mapListFromJson(Map<String, dynamic> json) {
    final map = Map<String, List<Client>>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) {
        map[key] = Client.listFromJson(value);
      });
    }
    return map;
  }
}

