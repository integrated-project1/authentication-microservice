part of userApiClient.api;

class AuthenticationInfo {
  
  String clientId;
  
  String clientSecret;
  
  String redirectUri;
  
  String responseType;
  
  String scope;
  
  String state;

  AuthenticationInfo({
    @required this.clientId,
    @required this.clientSecret,
    this.redirectUri,
    @required this.responseType,
    this.scope,
    this.state,
  });

  @override
  String toString() {
    return 'AuthenticationInfo[clientId=$clientId, clientSecret=$clientSecret, redirectUri=$redirectUri, responseType=$responseType, scope=$scope, state=$state, ]';
  }

  AuthenticationInfo.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    clientId = json['client_id'];
    clientSecret = json['client_secret'];
    redirectUri = json['redirect_uri'];
    responseType = json['response_type'];
    scope = json['scope'];
    state = json['state'];
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = {};
    if (clientId != null)
      json['client_id'] = clientId;
    if (clientSecret != null)
      json['client_secret'] = clientSecret;
    if (redirectUri != null)
      json['redirect_uri'] = redirectUri;
    if (responseType != null)
      json['response_type'] = responseType;
    if (scope != null)
      json['scope'] = scope;
    if (state != null)
      json['state'] = state;
    return json;
  }

  static List<AuthenticationInfo> listFromJson(List<dynamic> json) {
    return json == null ? List<AuthenticationInfo>() : json.map((value) => AuthenticationInfo.fromJson(value)).toList();
  }

  static Map<String, AuthenticationInfo> mapFromJson(Map<String, dynamic> json) {
    final map = Map<String, AuthenticationInfo>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = AuthenticationInfo.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of AuthenticationInfo-objects as value to a dart map
  static Map<String, List<AuthenticationInfo>> mapListFromJson(Map<String, dynamic> json) {
    final map = Map<String, List<AuthenticationInfo>>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) {
        map[key] = AuthenticationInfo.listFromJson(value);
      });
    }
    return map;
  }
}

