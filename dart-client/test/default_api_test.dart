import 'package:userApiClient/api.dart';
import 'package:test/test.dart';


/// tests for DefaultApi
void main() {
  var instance = DefaultApi();

  group('tests for DefaultApi', () {
    // Delete a client
    //
    //Future deleteClient(String clientId) async 
    test('test deleteClient', () async {
      // TODO
    });

    // Delete me
    //
    //Future deleteMe() async 
    test('test deleteMe', () async {
      // TODO
    });

    // Get a client's data
    //
    //Future<Client> getClientInfo(String clientId) async 
    test('test getClientInfo', () async {
      // TODO
    });

    // Get the list of the clients
    //
    //Future<List<String>> getClients() async 
    test('test getClients', () async {
      // TODO
    });

    // Get the public key
    //
    //Future<Object> getKey() async 
    test('test getKey', () async {
      // TODO
    });

    // Get form for logging in
    //
    //Future<String> getLoginForm(String responseType, String clientId, { String redirectUri, String scope, String state }) async 
    test('test getLoginForm', () async {
      // TODO
    });

    // Get my data
    //
    //Future<Client> getMyInfo() async 
    test('test getMyInfo', () async {
      // TODO
    });

    // Authentication, ask for a JWT
    //
    //Future login(AuthenticationInfo authenticationInfo) async 
    test('test login', () async {
      // TODO
    });

    // Register a new client
    //
    //Future register(Client client) async 
    test('test register', () async {
      // TODO
    });

    // Update a client's data
    //
    //Future updateClientInfo(String clientId, Client client) async 
    test('test updateClientInfo', () async {
      // TODO
    });

    // Update a my data
    //
    //Future updateMyInfo(Client client) async 
    test('test updateMyInfo', () async {
      // TODO
    });

  });
}
