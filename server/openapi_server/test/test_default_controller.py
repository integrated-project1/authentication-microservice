# coding: utf-8

from __future__ import absolute_import
import unittest

from flask import json
from six import BytesIO

from openapi_server.models.authentication_info import AuthenticationInfo  # noqa: E501
from openapi_server.models.client import Client  # noqa: E501
from openapi_server.test import BaseTestCase


class TestDefaultController(BaseTestCase):
    """DefaultController integration test stubs"""

    def test_delete_client(self):
        """Test case for delete_client

        Delete a client
        """
        token = self.login("admin", "lolilol")
        headers = {
            'Authorization': 'Bearer ' + token,
        }
        response = self.client.open(
            '/1.0.0/clients/{client_id}'.format(client_id='aaa'),
            method='DELETE',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_delete_me(self):
        """Test case for delete_me

        Delete me
        """
        token = self.login("aaa", "xxx")
        headers = { 
            'Authorization': 'Bearer ' + token,
        }
        response = self.client.open(
            '/1.0.0/me',
            method='DELETE',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_get_client_info(self):
        """Test case for get_client_info

        Get a client's data
        """
        token = self.login("admin", "lolilol")
        headers = { 
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + token,
        }
        response = self.client.open(
            '/1.0.0/clients/{client_id}'.format(client_id='aaa'),
            method='GET',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_get_clients(self):
        """Test case for get_clients

        Get the list of the clients
        """
        token = self.login("admin", "lolilol")
        headers = {
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + token,
        }
        response = self.client.open(
            '/1.0.0/clients',
            method='GET',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_get_key(self):
        """Test case for get_key

        Get the public key
        """
        headers = { 
            'Accept': 'application/json',
        }
        response = self.client.open(
            '/1.0.0/key',
            method='GET',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_get_login_form(self):
        """Test case for get_login_form

        Get form for logging in
        """
        query_string = [('response_type', "token"),
                        ('client_id', "xXxDarkSasukeKingSlayerdu62xXx"),
                        ('redirect_uri', "http://example.com:123/somestuff"),
                        ('scope', "read:clients delete:clients register:clients read:data"),
                        ('state', "U2F0IEp1bCAyNSAyMDIwIDEzOjMwOjIyIEdNVCswMjAwIChDZW50cmFsIEV1cm9wZWFuIFN1bW1lciBUaW1lKQ%3D%3D")]
        headers = {
            'Accept': 'text/html',
        }
        response = self.client.open(
            '/1.0.0/login',
            method='GET',
            headers=headers,
            query_string=query_string)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_get_my_info(self):
        """Test case for get_my_info

        Get my data
        """
        token = self.login("admin", "lolilol")
        headers = { 
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + token,
        }
        response = self.client.open(
            '/1.0.0/me',
            method='GET',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_login(self):
        """Test case for login

        Authentication, ask for a JWT
        """
        authentication_info = {
  "scope" : "read:clients delete:clients register:clients read:data",
  "response_type" : "token",
  "client_secret" : "lolilol",
  "redirect_uri" : "http://localhost:8080/clients",
  "state" : "U2F0IEp1bCAyNSAyMDIwIDEzOjMwOjIyIEdNVCswMjAwIChDZW50cmFsIEV1cm9wZWFuIFN1bW1lciBUaW1lKQ%3D%3D",
  "client_id" : "admin"
}
        headers = { 
            'Content-Type': 'application/json',
        }
        response = self.client.open(
            '/1.0.0/login',
            method='POST',
            headers=headers,
            data=json.dumps(authentication_info),
            content_type='application/json')
        #this only tests the redirect, not the token
        self.assertStatus(response, 302, 'Response body is : ' + response.data.decode('utf-8'))

    def test_register(self):
        """Test case for register

        Register a new client
        """
        client = {
  "scope" : "read:clients delete:clients register:clients read:data",
  "client_secret" : "1234",
  "client_type" : "public",
  "redirect_uris" : "http://example.com/ici https://instance.org/la",
  "client_id" : "xXxDarkSasukeKingSlayerdu62xXx"
}
        token = self.login("admin", "lolilol")
        headers = { 
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token,
        }
        response = self.client.open(
            '/1.0.0/clients',
            method='POST',
            headers=headers,
            data=json.dumps(client),
            content_type='application/json')
        self.assertStatus(response, 201,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_update_client_info(self):
        """Test case for update_client_info

        Update a client's data
        """
        client = {
  "scope" : "read:clients delete:clients register:clients read:data",
  "client_secret" : "1234",
  "client_type" : "public",
  "redirect_uris" : "http://example.com/ici https://instance.org/la",
  "client_id" : "xXxDarkSasukeKingSlayerdu62xXx"
}
        token = self.login("admin", "lolilol")
        headers = { 
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '  + token,
        }
        response = self.client.open(
            '/1.0.0/clients/{client_id}'.format(client_id="aaa"),
            method='PUT',
            headers=headers,
            data=json.dumps(client),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_update_my_info(self):
        """Test case for update_my_info

        Update a my data
        """
        client = {
  "scope" : "read:clients delete:clients register:clients read:data",
  "client_secret" : "1234",
  "client_type" : "public",
  "redirect_uris" : "http://example.com/ici https://instance.org/la",
  "client_id" : "aaa"
}
        token = self.login("aaa", "xxx")
        headers = { 
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token,
        }
        response = self.client.open(
            '/1.0.0/me',
            method='PUT',
            headers=headers,
            data=json.dumps(client),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    unittest.main()
