import logging

import connexion
from flask_testing import TestCase

from openapi_server.encoder import JSONEncoder
from openapi_server import db
from openapi_server.models.client import Client
from flask_sqlalchemy import SQLAlchemy
from openapi_server.__main__ import init_db

import json
import os

class BaseTestCase(TestCase):

    """See https://pythonhosted.org/Flask-Testing/
    """
    
    def create_app(self):
        logging.getLogger('connexion.operation').setLevel('ERROR')
        app = connexion.App(__name__, specification_dir='../openapi/')
        app.app.config['SQLALCHEMY_DATABASE_URI'] = \
		f"""{os.environ['DATABASE_TYPE']}://\
		{os.environ['DATABASE_USER']}:\
		{os.environ['DATABASE_PASSWORD']}@\
		{os.environ['DATABASE_HOST']}/\
		{os.environ['DATABASE_NAME']}"""
        app.app.json_encoder = JSONEncoder
        app.app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = 'False'
        with app.app.app_context():
            db.init_app(app.app)
        init_db()
        app.add_api('openapi.yaml', pythonic_params=True)
        return app.app

    def login(self, username, password):
        """login as username with password password, returns the bearer token
        """
        headers = { 
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
        user = {
  			"client_secret" : password,
  			"client_id" : username,
                        "response_type": "token"
		}

        response = self.client.open(
            '/1.0.0/login',
            method='POST',
            headers=headers,
            data=json.dumps(user),
            content_type='application/json',
            follow_redirects=False)
        #One very shitty line that takes the value of the first argument of the fragment
        access_token = response.headers['Location'].split("#")[1].split('&')[0].split('=')[1]
        return access_token