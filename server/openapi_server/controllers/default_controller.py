import connexion
import six

from openapi_server.models.authentication_info import AuthenticationInfo  # noqa: E501
from openapi_server.models.client import Client  # noqa: E501
from openapi_server import util
from openapi_server import db

import openapi_server.controllers.security_controller_ as security

def delete_client(client_id):  # noqa: E501
    """Delete a client

     # noqa: E501

    :param client_id: 
    :type client_id: str

    :rtype: None
    """
    return _delete_client(client_id)

def delete_me():  # noqa: E501
    """Delete me

     # noqa: E501


    :rtype: None
    """
    return _delete_client(connexion.context['token_info']['client_id'])

def _delete_client(client_id):
    """Delete a client
    
    :param client_id: client_id of the client to be deleted 
    :type client_id: str
    
    :rtype: None
    """
    if client_id == 'admin':
        return 'admin cannot be deleted', 403

    client = Client.query.filter_by(client_id=client_id).first()
    if client is None:
        return 'Client does not exist', 204

    db.session.delete(client)
    db.session.commit()
    return 'Client successfully deleted', 200

def get_key():  # noqa: E501
    """Get the public key

     # noqa: E501


    :rtype: object
    """
    return {'key': security.PUBLIC_KEY}, 200

def get_login_form(response_type, client_id, redirect_uri=None, scope=None, state=None):  # noqa: E501
    """Get form for logging in
    
    # noqa: E501

    :param response_type: Type of the response. Must be set to \&quot;token\&quot; in the context of Oauth2 implicit flow
    :type response_type: str
    :param client_id: ID of the client wanting to log in
    :type client_id: str
    :param redirect_uri: URI to which the client will be redirected after authentication
    :type redirect_uri: str
    :param scope: Space separeted list of scopes the token will have
    :type scope: str
    :param state: An opaque value used by the client to maintain state between the request and callback
    :type state: str

    :rtype: str
    """
    
    ret = """<form action="/1.0.0/login" method="post">
  <label for="client_id">ID:</label><br>
  <input type="text" id="client_id" name="client_id" value="{}"><br>
  <label for="psw">Password:</label><br>
  <input type="text" id="psw" name="psw" value=""><br><br>
  <input type="submit" value="Submit">
</form>""".format(client_id)

    return ret
    
    return '''{% extends "bootstrap/base.html" %}
{% block content %}
    {% with messages = get_flashed_messages(with_categories=true) %}
        {% if messages %}
            <div class="alert alert-{{ messages[0][0] }}">{{ messages[0][1] }}</div>
            <script>
                console.log("{{ messages[0][0] }}");
                if ("{{ messages[0][0] }}" === "success") {
                    console.log("refresh");
                    window.top.location.reload();
                }
            </script>
        {% endif %}
    {% endwith %}
    <form action="" method="post" class="form" novalidate>
        {{ form.hidden_tag() }}
        <p>
            {{ form.username.label }}<br>
            {{ form.username(class_="form-control") }}
        </p>
        <p>
            {{ form.password.label }}<br>
            {{ form.password(class_="form-control") }}
        </p>
        <p>{{ form.submit(class_="btn btn-primary")}}</p>
    </form>
{% endblock %}'''

def get_my_info():  # noqa: E501
    """Get my data

     # noqa: E501


    :rtype: Client
    """
    return _get_client_info(connexion.context['token_info']['client_id'])

def get_client_info(client_id):  # noqa: E501
    """Get a client&#39;s data

     # noqa: E501

    :param client_id: 
    :type client_id: str

    :rtype: Client
    """
    return _get_client_info(client_id)

def _get_client_info(client_id):
    """Get a client&#39;s data

    :param client_id: client_id of the client whose data if getting fetched
    :type client_id: str

    :rtype: Client
    """
    client = Client.query.filter_by(client_id=client_id).first()
    if client is None:
        return 'Client does not exist', 204
    
    #remove 'client_secret'
    client_dict = client.to_dict()
    del client_dict['client_secret']
    return Client.from_dict(client_dict), 200

def get_clients():  # noqa: E501
    """Get the list of the clients

     # noqa: E501


    :rtype: List[str]
    """
    clients = Client.query.all()
    return list(map(lambda x : x.client_id, clients)), 200


def login(body):  # noqa: E501
    """Login, ask for a JWT

     # noqa: E501

    :param client: Client credentials for logging in
    :type client: dict | bytes

    :rtype: object
    """
    if connexion.request.is_json:
        authentication_info = AuthenticationInfo.from_dict(connexion.request.get_json())

    # Check the validity of the request
    if authentication_info.response_type != 'token':
        return "Response type must be 'token' in implicit flow"
    
    # Check credentials
    client = Client.query.filter_by(client_id=authentication_info.client_id).first()
    if client is None or client.verify_pass(authentication_info.client_secret) is False:
        return 'Invalid credentials', 422

    # Check scoping
    if authentication_info.scope is not None:
        required_scope = authentication_info.scope.split()
        client_scope = client.scope.split()
        if not set(required_scope).issubset(set(client_scope)):
            return 'You do not have these permissions', 422
        else:
            scope = authentication_info.scope
    else:
        scope = client.scope

    # Checking redirection
    if authentication_info.redirect_uri is not None:
        if authentication_info.redirect_uri not in client.redirect_uris.split():
            return 'Invalid redirection uri', 422
        else:
            redirection_uri = authentication_info.redirect_uri
    else:
        # Return first uri is none is provided
        redirection_uri = client.redirect_uris.split()[0]

    # At this point, credentials are correct
    token = security.create_token({
        'client_id': client.client_id,
        'scope': scope
        })

    # Adding fragments to URI
    location = redirection_uri + "#access_token=" + token + "&token_type=" + \
        security.TOKEN_TYPE + "&expires_in=" + str(security.TOKEN_EXPIRATION_TIME) + "&scope=" + scope \
        + ("&state=" + authentication_info.state if authentication_info.state is not None else "")
    
    return None, 302, {'Location': location}

def register(body):  # noqa: E501
    """Register a new client

     # noqa: E501

    :param client: Client credentials for registering
    :type client: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        client = Client.from_dict(connexion.request.get_json())  # noqa: E501

    for attr, _ in six.iteritems(client.openapi_types):
        value = getattr(client, attr)
        if value is None:
            return "'{}' field is a required property".format(attr), 400

    # Request body id valid
    client.client_secret = Client.hash_pass(client.client_secret)

    if Client.query.filter_by(client_id=client.client_id).first() is not None:
        return 'Duplicate client', 409

    db.session.add(client)
    db.session.commit()

    return 'Client successfully registered', 201

def update_my_info(body):  # noqa: E501
    """Update a my data

     # noqa: E501

    :param client: Client credentials for registering
    :type client: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        client = Client.from_dict(connexion.request.get_json())  # noqa: E501
        if client.client_secret is not None:
            client.client_secret = Client.hash_pass(client.client_secret)

    #self modification of scope is forbidden
    client.scope = None

    return _update_client_info(connexion.context['token_info']['client_id'], client)

def update_client_info(client_id, body):  # noqa: E501
    """Update a client's data

     # noqa: E501

    :param client_id: 
    :type client_id: str
    :param client: Client credentials for registering
    :type client: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        client = Client.from_dict(connexion.request.get_json())  # noqa: E501
        if client.client_secret is not None:
            client.client_secret = Client.hash_pass(client.client_secret)

    return _update_client_info(client_id, client)

def _update_client_info(client_id, client):
    """Update a client's data

    :param client_id: 
    :type client_id: str
    :param client: new client that will replace the old one
    :type client: Client

    :rtype: None
    """
    # Protecting admin
    if client_id == "admin":
        for attr in ['client_id', 'scope', 'client_type']:
            value = getattr(client, attr)
            if value is not None:
                return 'admin {} cannot be modified'.format(attr), 403

    # Same client_id
    if client.client_id is not None and client_id != client.client_id:
        suspect_client = Client.query.filter_by(client_id=client.client_id).first()
        if suspect_client is not None:
            return 'Duplicate client', 409

    # The client may be modified
    old_client = Client.query.filter_by(client_id=client_id).first()

    if old_client is None:
        return 'Client does not exist', 204

    for attr, _ in six.iteritems(old_client.openapi_types):
        new_value = getattr(client, attr)
        if new_value is not None:
            setattr(old_client, attr, new_value)

    db.session.commit()
    return 'Client successfully modified', 200
