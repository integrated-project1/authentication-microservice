import jwt
import time

from typing import List

import os


PRIVATE_KEY = open(os.environ["JWT_PRIVATE_KEY_FILE"]).read()
PUBLIC_KEY = open(os.environ["JWT_PUBLIC_KEY_FILE"]).read()
SIGNATURE_ALGORITHM = 'RS256'
TOKEN_EXPIRATION_TIME = 3600
TOKEN_TYPE = 'bearer'

def decode_token(token):
    """
    Validate and decode token.
    Returned value will be passed in 'token_info' parameter of your operation function, if there is one.
    'sub' or 'uid' will be set in 'user' parameter of your operation function, if there is one.
    'scope' or 'scopes' will be passed to scope validation function.

    :param token: Token provided by Authorization header
    :type token: str
    :return: Decoded token information or None if token is invalid
    :rtype: dict | None
    """
    try:
        payload = jwt.decode(token, PUBLIC_KEY, algorithms=[SIGNATURE_ALGORITHM])

        # In order to be compliant with Connexion and RFC 7662
        payload['active'] = True if payload['exp'] < int(time.time()) else False
        return payload
    except jwt.exceptions.InvalidTokenError:
        return None


def create_token(info):
    """
    Create a bearer token from authentication information.

    :param info: User info to create the bearer token from
    :type info: dict
    :return: A bearer token
    :rtype: str
    """
    now = int(time.time())
    expiration_time = now + TOKEN_EXPIRATION_TIME

    payload = {
        'client_id': info['client_id'],
        'token_type': TOKEN_TYPE,
        'scope': info['scope'],
        'iat': now,
        'exp': expiration_time
    }
    return jwt.encode(payload, PRIVATE_KEY, algorithm=SIGNATURE_ALGORITHM).decode('utf-8')
