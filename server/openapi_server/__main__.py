#!/usr/bin/env python3

import os

from openapi_server import app
from openapi_server import db
from openapi_server.models.client import Client

def init_db():
    """Initializes the db for development purposes
    """
    db.drop_all()
    db.create_all()
    db.session.commit()
    db.session.add(Client(client_id='admin', client_secret=Client.hash_pass('lolilol'), scope='update:clients update:self read:self read:clients delete:self read:self delete:clients register:clients write:buildings update:buildings delete:buildings read:data write:data', client_type='public', redirect_uris='http://localhost:8080/clients http://localhost:8080/clients/aaa'))
    db.session.add(Client(client_id='aaa', client_secret=Client.hash_pass('xxx'), scope='read:clients delete:self read:self update:self',
        client_type='public', redirect_uris='http://localhost:8080/clients http://localhost:8080/clients/aaa'))
    db.session.add(Client(client_id='user_test', client_secret=Client.hash_pass('topsecret'), scope='update:clients update:self read:self read:clients delete:self read:self delete:clients register:clients write:buildings update:buildings delete:buildings read:data write:data',
        client_type='public', redirect_uris='http://localhost:8080/clients http://localhost:8080/clients/user_test'))
    db.session.add(Client(client_id='user_test_unprivileged', client_secret=Client.hash_pass('topsecret'), scope='',
        client_type='public', redirect_uris='http://localhost:8080/clients http://localhost:8080/clients/user_test_unprivileged'))
    db.session.commit()


if __name__ == '__main__':
    if os.environ['INIT_DB'].upper() == "YES":
        init_db()
    app.run(port=os.environ['OAUTH2_PORT'])
