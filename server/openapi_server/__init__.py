
import connexion
import os
from flask_sqlalchemy import SQLAlchemy

from openapi_server import encoder

app = connexion.App(__name__, specification_dir='./openapi/')
app.app.config['SQLALCHEMY_DATABASE_URI'] = \
f"""{os.environ['DATABASE_TYPE']}://\
{os.environ['DATABASE_USER']}:\
{os.environ['DATABASE_PASSWORD']}@\
{os.environ['DATABASE_HOST']}/\
{os.environ['DATABASE_NAME']}"""

db = SQLAlchemy(app.app)

app.app.json_encoder = encoder.JSONEncoder
app.add_api('openapi.yaml',
            arguments={'title': 'Electrical energy at a glance authentication service API'},
            pythonic_params=True)

db.create_all()
db.session.commit()

if(os.environ["INIT_ADMIN"].upper() == "YES"):
	from openapi_server.models.client import Client
	db.session.add(Client(client_id='admin', client_secret=Client.hash_pass(os.environ['ADMIN_PWD']),
		scope='update:clients update:self read:self read:clients delete:self read:self delete:clients register:clients write:buildings update:buildings delete:buildings read:data write:data',
		client_type='public', redirect_uris=os.environ['ADMIN_URI']))
	db.session.commit()
