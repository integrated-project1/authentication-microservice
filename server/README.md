# Authentication server

## Overview
The skeleton of this server has been generated using the openapi generator. The framework is connexion (which is built on top of flask which itself is built on top of werkzeug)

## Running with Docker

To run the server you can pull the image from the gitlab container registry. However some configuration is in order

# Configuration

## Mandatory

The authentication server needs a database to keep track of users. For that reason the docker container must be supplied with the following environment variables:
 - DATABASE_NAME: the name of the database which will store user information
 - DATABASE_HOST: the actual host of the database
 - DATABASE_USER: the user used by the authentication server to interact with the database
 - DATABASE_PASSWORD: The password of the previously mentioned database user
 - DATABASE_TYPE: The type of the database. Be mindful that this type of database should be supported by sqlalchemy

## Very recommended

 - Change the keys used by the authentication server. You will see that a pair of keys has been uploaded to the repository. These should absolutely be changed by using mounts or volumes. Their paths within the container defaults to the same as this repository. However you are free to change these by overriding the default values of the environment variables JWT_PUBLIC_KEY_FILE and JWT_PRIVATE_KEY_FILE.
 - Change the default users, obviously you don't want to have people logging in with them
## Why not

 - The port listened to (and exposed) by the server is 8080 by default but can be overriden by setting the environment variable OAUTH2_PORT. However you could always 